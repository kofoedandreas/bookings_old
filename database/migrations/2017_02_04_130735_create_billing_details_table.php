<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_details', function (Blueprint $table) {
            $table->increments('billingDetailsId');
            $table->string('company', 30);
            $table->string('address', 50);
            $table->string('city', 20);
            $table->smallInteger('zip');
            $table->smallInteger('cvr');
            $table->smallInteger('ean_no');
            $table->string('billing_name', 50);
            $table->string('billing_address', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_details');
    }
}
