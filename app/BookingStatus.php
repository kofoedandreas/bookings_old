<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingStatus extends Model
{
      /**
     * Get the bookings for the booking status.
     */
      public function booking()
      {
      	return $this->hasMany('App\Booking');
      }
}
