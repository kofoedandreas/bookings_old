<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingDetail extends Model
{
     /**
     * Get the customer that owns the booking.
     */
     public function customer()
     {
     	return $this->belongsTo('App\Customer');
     }
 }
