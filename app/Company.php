<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * Get the customers (employees) for the company.
     */
    public function customers()
    {
    	return $this->hasMany('App\Customer');
    }
}
