<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Get the customer that owns the order.
     */
    public function customer()
    {
    	return $this->belongsTo('App\Customer');
    }

    /**
     * Get the course that owns the order.
     */
    public function course()
    {
    	return $this->belongsTo('App\Course');
    }

      /**
     * Get the booking that owns the order.
     */
      public function booking()
      {
      	return $this->belongsTo('App\Booking');
      }
  }
