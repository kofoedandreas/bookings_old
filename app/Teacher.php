<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
      /**
     * Get the course that the teacher teach.
     */
      public function course()
      {
      	return $this->belongsTo('App\Course');
      }
  }
