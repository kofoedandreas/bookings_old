<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * Get the associated billing details.
     */
    public function billingDetails()
    {
    	return $this->hasOne('App\BillingDetails');
    }

      /**
     * Get the company that owns the customer (employee).
     */
      public function company()
      {
      	return $this->belongsTo('App\Company');
      }

      /**
     * Get the bookings for the course.
     */
      public function booking()
      {
      	return $this->hasMany('App\Booking');
      }

       /**
     * Get the orders for the course.
     */
       public function order()
       {
       	return $this->hasMany('App\Order');
       }
   }
