<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
     /**
     * Get the course that owns the booking.
     */
     public function course()
     {
     	return $this->belongsTo('App\Course');
     }

      /**
     * Get the customer that owns the booking.
     */
      public function customer()
      {
      	return $this->belongsTo('App\Customer');
      }

       /**
     * Get the booking_status for the booking.
     */
       public function bookingStatus()
       {
       	return $this->belongsTo('App\BookingStatus');
       }

      /**
     * Get the order record associated with the booking.
     */
      public function order()
      {
      	return $this->hasOne('App\Order');
      }
  }
