<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
     /**
     * Get the course that owns the date.
     */
     public function course()
     {
     	return $this->belongsTo('App\Course');
     }
 }
