<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
      /**
     * Get the course that owns the booking.
     */
      public function course()
      {
      	return $this->belongsTo('App\Course');
      }
  }
