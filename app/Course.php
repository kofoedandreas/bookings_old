<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
     /**
     * Get the teachers for the course.
     */
     public function teachers()
     {
     	return $this->hasMany('App\Teacher');
     }

     /**
     * Get the dates for the course.
     */
     public function dates()
     {
     	return $this->hasMany('App\Date');
     }

     /**
     * Get the bookings for the course.
     */
     public function bookings()
     {
     	return $this->hasMany('App\Booking');
     }

     /**
     * Get the cities for the course.
     */
     public function cities()
     {
     	return $this->hasMany('App\City');
     }

     /**
     * Get the ratings for the course.
     */
     public function ratings()
     {
     	return $this->hasMany('App\Rating');
     }
 }
